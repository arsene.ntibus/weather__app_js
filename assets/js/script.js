/**
 * @let  allows me to
 */
let city = ""

/**
 * @func Allows me to parse city to the API.
 */
function citySearch(city) {
    console.log("city search = " + city)
    /**
     * I initiate the API URL in which  I concatenate the empty variable + add the option to  have the
     * appent informations in French.
     */
    let API_URL = "https://api.openweathermap.org/data/2.5/weather?q=" + city
        + "&lang=fr&units=metric&appid=296e3af7970883f78969c4451135e3f7";
    /**
     * Starting  to  fetch the API
     */
    fetch(API_URL)
        //
        .then(function (res) {
            /**
             * @res If the Http header response sends an OK Status , i append the ReadableStream  through the body property in the console.
             */
            if (res.ok) {
                console.log(res.body);
                // I  the  transform the response into a JSON object  to be able to fetch the  core key to display th
                return res.json();
            }
        })
        /**
         * @then From JSON Object I   get  the Following properties and display them in the front.
         */
        .then(function (regions){
            console.log(regions);
            document.getElementById('cityName').innerHTML = regions.name
            document.getElementById('temp').innerHTML = "Actuellemennt  : " + regions.main.feels_like + "°C";
            document.getElementById('felt').innerHTML = regions.main.feels_like
            document.getElementById('max').innerHTML = regions.main.temp_max
            document.getElementById('min').innerHTML = regions.main.temp_min
            document.getElementById('humidity').innerHTML = regions.main.humidity
            document.getElementById('windspeed').innerHTML = regions.wind.speed
        })
        .catch(function (err) {
            // An error Happened
            console.log(err, "error");
        });
}

function resolveAfter2Seconds(city) {
    return new Promise(resolve => {
        setTimeout(() => {
            citySearch(city)
            console.log(resolve)
            resolve('resolved');
        }, 200);
    });

    async function asyncCall(city) {
        if (city) { //
            console.log('calling');
            const result = await resolveAfter2Seconds(city);
            console.log(result);
        } else {
            document.getElementById('temp').innerHTML = "Ecrivez une ville avant de valider..."
            document.getElementById('temp').style.fontweight = 'bold';
            document.getElementById('temp').style.color = 'red';
        }
        // expected output: "resolved"
    }
}